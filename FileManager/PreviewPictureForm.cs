﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace FileManager
{
    public partial class PreviewPictureForm : Form
    {
        public PreviewPictureForm()
        {
            InitializeComponent();
            this.LostFocus += new EventHandler(OnLostFocusEvent);
        }

        private void OnLostFocusEvent(object sender, EventArgs e)
        {
            this.Hide();
        }

        public void PlaceUnderSelectedItem(ListView listView)
        {
            //TODO: move this to a base class!!!
            if (listView != null && listView.SelectedItems.Count == 1 && listView.SelectedIndices.Count == 1)
            {
                // Setup local position (onto list view form)
                var localPos = listView.SelectedItems[0].Position;
                localPos.Y += listView.GetItemRect(listView.SelectedIndices[0]).Height;

                // Setup global position (onto screen)
                var globalPos = listView.PointToScreen(localPos);
                this.Location = globalPos;
            }
        }

        public bool ShowFileContent(string filename)
        {
            const int frameBorder = 5;
            const int maxWidth = 600;
            const int maxHeight = 500;

            // Load image and adjust size if necessary
            var image = Image.FromFile(filename);
            var size = image.Size;

            Helper.ClampSize(ref size, new Size(maxWidth, maxHeight));
            if (size != image.Size)
                image = Helper.ResizeImage(image, size);

            // Set new image
            pictureBox.Image = image;
            this.ClientSize = pictureBox.Image.Size + new Size(frameBorder, frameBorder);

            return true;
        }
    }
}
