﻿namespace FileManager
{
    partial class ViewOptionsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ViewOptionsForm));
            this.buttonOK = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.panelOptions = new System.Windows.Forms.Panel();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPageCommon = new System.Windows.Forms.TabPage();
            this.groupHome = new System.Windows.Forms.GroupBox();
            this.buttonHome = new System.Windows.Forms.Button();
            this.textHome = new System.Windows.Forms.TextBox();
            this.tabPageView = new System.Windows.Forms.TabPage();
            this.optHideUnknownFileType = new System.Windows.Forms.CheckBox();
            this.optAlternateEntryColor = new System.Windows.Forms.CheckBox();
            this.optShowHiddenFiles = new System.Windows.Forms.CheckBox();
            this.optUseHighlighting = new System.Windows.Forms.CheckBox();
            this.optShowExtFolders = new System.Windows.Forms.CheckBox();
            this.optShowKBOnly = new System.Windows.Forms.CheckBox();
            this.optUseMonthNames = new System.Windows.Forms.CheckBox();
            this.optHideKnownFileExt = new System.Windows.Forms.CheckBox();
            this.optUsePrettyDays = new System.Windows.Forms.CheckBox();
            this.optShowSeconds = new System.Windows.Forms.CheckBox();
            this.optShowFoldersFirst = new System.Windows.Forms.CheckBox();
            this.tabPageColors = new System.Windows.Forms.TabPage();
            this.buttonExecutableColor = new System.Windows.Forms.Button();
            this.labelExecutableColor = new System.Windows.Forms.Label();
            this.buttonFolderColor = new System.Windows.Forms.Button();
            this.labelFolderColor = new System.Windows.Forms.Label();
            this.colorPicker = new System.Windows.Forms.ColorDialog();
            this.panelOptions.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.tabPageCommon.SuspendLayout();
            this.groupHome.SuspendLayout();
            this.tabPageView.SuspendLayout();
            this.tabPageColors.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonOK
            // 
            resources.ApplyResources(this.buttonOK, "buttonOK");
            this.buttonOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.UseVisualStyleBackColor = true;
            // 
            // buttonCancel
            // 
            resources.ApplyResources(this.buttonCancel, "buttonCancel");
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            // 
            // panelOptions
            // 
            this.panelOptions.Controls.Add(this.buttonCancel);
            this.panelOptions.Controls.Add(this.buttonOK);
            this.panelOptions.Controls.Add(this.tabControl);
            resources.ApplyResources(this.panelOptions, "panelOptions");
            this.panelOptions.Name = "panelOptions";
            // 
            // tabControl
            // 
            resources.ApplyResources(this.tabControl, "tabControl");
            this.tabControl.Controls.Add(this.tabPageCommon);
            this.tabControl.Controls.Add(this.tabPageView);
            this.tabControl.Controls.Add(this.tabPageColors);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            // 
            // tabPageCommon
            // 
            this.tabPageCommon.Controls.Add(this.groupHome);
            resources.ApplyResources(this.tabPageCommon, "tabPageCommon");
            this.tabPageCommon.Name = "tabPageCommon";
            this.tabPageCommon.UseVisualStyleBackColor = true;
            // 
            // groupHome
            // 
            resources.ApplyResources(this.groupHome, "groupHome");
            this.groupHome.Controls.Add(this.buttonHome);
            this.groupHome.Controls.Add(this.textHome);
            this.groupHome.Name = "groupHome";
            this.groupHome.TabStop = false;
            // 
            // buttonHome
            // 
            resources.ApplyResources(this.buttonHome, "buttonHome");
            this.buttonHome.Name = "buttonHome";
            this.buttonHome.UseVisualStyleBackColor = true;
            this.buttonHome.Click += new System.EventHandler(this.buttonHome_Click);
            // 
            // textHome
            // 
            resources.ApplyResources(this.textHome, "textHome");
            this.textHome.Name = "textHome";
            this.textHome.ReadOnly = true;
            // 
            // tabPageView
            // 
            this.tabPageView.Controls.Add(this.optHideUnknownFileType);
            this.tabPageView.Controls.Add(this.optAlternateEntryColor);
            this.tabPageView.Controls.Add(this.optShowHiddenFiles);
            this.tabPageView.Controls.Add(this.optUseHighlighting);
            this.tabPageView.Controls.Add(this.optShowExtFolders);
            this.tabPageView.Controls.Add(this.optShowKBOnly);
            this.tabPageView.Controls.Add(this.optUseMonthNames);
            this.tabPageView.Controls.Add(this.optHideKnownFileExt);
            this.tabPageView.Controls.Add(this.optUsePrettyDays);
            this.tabPageView.Controls.Add(this.optShowSeconds);
            this.tabPageView.Controls.Add(this.optShowFoldersFirst);
            resources.ApplyResources(this.tabPageView, "tabPageView");
            this.tabPageView.Name = "tabPageView";
            this.tabPageView.UseVisualStyleBackColor = true;
            // 
            // optHideUnknownFileType
            // 
            resources.ApplyResources(this.optHideUnknownFileType, "optHideUnknownFileType");
            this.optHideUnknownFileType.Checked = true;
            this.optHideUnknownFileType.CheckState = System.Windows.Forms.CheckState.Checked;
            this.optHideUnknownFileType.Name = "optHideUnknownFileType";
            this.optHideUnknownFileType.UseVisualStyleBackColor = true;
            this.optHideUnknownFileType.CheckedChanged += new System.EventHandler(this.optHideUnknownFileType_CheckedChanged);
            // 
            // optAlternateEntryColor
            // 
            resources.ApplyResources(this.optAlternateEntryColor, "optAlternateEntryColor");
            this.optAlternateEntryColor.Checked = true;
            this.optAlternateEntryColor.CheckState = System.Windows.Forms.CheckState.Checked;
            this.optAlternateEntryColor.Name = "optAlternateEntryColor";
            this.optAlternateEntryColor.UseVisualStyleBackColor = true;
            this.optAlternateEntryColor.CheckedChanged += new System.EventHandler(this.optAlternateEntryColor_CheckedChanged);
            // 
            // optShowHiddenFiles
            // 
            resources.ApplyResources(this.optShowHiddenFiles, "optShowHiddenFiles");
            this.optShowHiddenFiles.Name = "optShowHiddenFiles";
            this.optShowHiddenFiles.UseVisualStyleBackColor = true;
            this.optShowHiddenFiles.CheckedChanged += new System.EventHandler(this.optShowHiddenFiles_CheckedChanged);
            // 
            // optUseHighlighting
            // 
            resources.ApplyResources(this.optUseHighlighting, "optUseHighlighting");
            this.optUseHighlighting.Checked = true;
            this.optUseHighlighting.CheckState = System.Windows.Forms.CheckState.Checked;
            this.optUseHighlighting.Name = "optUseHighlighting";
            this.optUseHighlighting.UseVisualStyleBackColor = true;
            this.optUseHighlighting.CheckedChanged += new System.EventHandler(this.optUseHighlighting_CheckedChanged);
            // 
            // optShowExtFolders
            // 
            resources.ApplyResources(this.optShowExtFolders, "optShowExtFolders");
            this.optShowExtFolders.Name = "optShowExtFolders";
            this.optShowExtFolders.UseVisualStyleBackColor = true;
            this.optShowExtFolders.CheckedChanged += new System.EventHandler(this.optShowExtFolders_CheckedChanged);
            // 
            // optShowKBOnly
            // 
            resources.ApplyResources(this.optShowKBOnly, "optShowKBOnly");
            this.optShowKBOnly.Checked = true;
            this.optShowKBOnly.CheckState = System.Windows.Forms.CheckState.Checked;
            this.optShowKBOnly.Name = "optShowKBOnly";
            this.optShowKBOnly.UseVisualStyleBackColor = true;
            this.optShowKBOnly.CheckedChanged += new System.EventHandler(this.optShowKBOnly_CheckedChanged);
            // 
            // optUseMonthNames
            // 
            resources.ApplyResources(this.optUseMonthNames, "optUseMonthNames");
            this.optUseMonthNames.Checked = true;
            this.optUseMonthNames.CheckState = System.Windows.Forms.CheckState.Checked;
            this.optUseMonthNames.Name = "optUseMonthNames";
            this.optUseMonthNames.UseVisualStyleBackColor = true;
            this.optUseMonthNames.CheckedChanged += new System.EventHandler(this.optUseMonthNames_CheckedChanged);
            // 
            // optHideKnownFileExt
            // 
            resources.ApplyResources(this.optHideKnownFileExt, "optHideKnownFileExt");
            this.optHideKnownFileExt.Name = "optHideKnownFileExt";
            this.optHideKnownFileExt.UseVisualStyleBackColor = true;
            this.optHideKnownFileExt.CheckedChanged += new System.EventHandler(this.optHideKnownFileExt_CheckedChanged);
            // 
            // optUsePrettyDays
            // 
            resources.ApplyResources(this.optUsePrettyDays, "optUsePrettyDays");
            this.optUsePrettyDays.Checked = true;
            this.optUsePrettyDays.CheckState = System.Windows.Forms.CheckState.Checked;
            this.optUsePrettyDays.Name = "optUsePrettyDays";
            this.optUsePrettyDays.UseVisualStyleBackColor = true;
            this.optUsePrettyDays.CheckedChanged += new System.EventHandler(this.optUsePrettyDays_CheckedChanged);
            // 
            // optShowSeconds
            // 
            resources.ApplyResources(this.optShowSeconds, "optShowSeconds");
            this.optShowSeconds.Name = "optShowSeconds";
            this.optShowSeconds.UseVisualStyleBackColor = true;
            this.optShowSeconds.CheckedChanged += new System.EventHandler(this.optShowSeconds_CheckedChanged);
            // 
            // optShowFoldersFirst
            // 
            resources.ApplyResources(this.optShowFoldersFirst, "optShowFoldersFirst");
            this.optShowFoldersFirst.Checked = true;
            this.optShowFoldersFirst.CheckState = System.Windows.Forms.CheckState.Checked;
            this.optShowFoldersFirst.Name = "optShowFoldersFirst";
            this.optShowFoldersFirst.UseVisualStyleBackColor = true;
            this.optShowFoldersFirst.CheckedChanged += new System.EventHandler(this.optShowFoldersFirst_CheckedChanged);
            // 
            // tabPageColors
            // 
            this.tabPageColors.Controls.Add(this.buttonExecutableColor);
            this.tabPageColors.Controls.Add(this.labelExecutableColor);
            this.tabPageColors.Controls.Add(this.buttonFolderColor);
            this.tabPageColors.Controls.Add(this.labelFolderColor);
            resources.ApplyResources(this.tabPageColors, "tabPageColors");
            this.tabPageColors.Name = "tabPageColors";
            this.tabPageColors.UseVisualStyleBackColor = true;
            // 
            // buttonExecutableColor
            // 
            this.buttonExecutableColor.BackColor = System.Drawing.Color.ForestGreen;
            resources.ApplyResources(this.buttonExecutableColor, "buttonExecutableColor");
            this.buttonExecutableColor.Name = "buttonExecutableColor";
            this.buttonExecutableColor.UseVisualStyleBackColor = false;
            this.buttonExecutableColor.Click += new System.EventHandler(this.buttonExecutableColor_Click);
            // 
            // labelExecutableColor
            // 
            resources.ApplyResources(this.labelExecutableColor, "labelExecutableColor");
            this.labelExecutableColor.Name = "labelExecutableColor";
            // 
            // buttonFolderColor
            // 
            this.buttonFolderColor.BackColor = System.Drawing.Color.Blue;
            resources.ApplyResources(this.buttonFolderColor, "buttonFolderColor");
            this.buttonFolderColor.Name = "buttonFolderColor";
            this.buttonFolderColor.UseVisualStyleBackColor = false;
            this.buttonFolderColor.Click += new System.EventHandler(this.buttonFolderColor_Click);
            // 
            // labelFolderColor
            // 
            resources.ApplyResources(this.labelFolderColor, "labelFolderColor");
            this.labelFolderColor.Name = "labelFolderColor";
            // 
            // ViewOptionsForm
            // 
            this.AcceptButton = this.buttonOK;
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.buttonCancel;
            this.Controls.Add(this.panelOptions);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ViewOptionsForm";
            this.ShowIcon = false;
            this.Load += new System.EventHandler(this.ViewOptionsForm_Load);
            this.panelOptions.ResumeLayout(false);
            this.tabControl.ResumeLayout(false);
            this.tabPageCommon.ResumeLayout(false);
            this.groupHome.ResumeLayout(false);
            this.groupHome.PerformLayout();
            this.tabPageView.ResumeLayout(false);
            this.tabPageView.PerformLayout();
            this.tabPageColors.ResumeLayout(false);
            this.tabPageColors.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Panel panelOptions;
        private System.Windows.Forms.CheckBox optShowHiddenFiles;
        private System.Windows.Forms.CheckBox optShowExtFolders;
        private System.Windows.Forms.CheckBox optUseMonthNames;
        private System.Windows.Forms.CheckBox optUsePrettyDays;
        private System.Windows.Forms.CheckBox optShowSeconds;
        private System.Windows.Forms.CheckBox optShowFoldersFirst;
        private System.Windows.Forms.CheckBox optHideUnknownFileType;
        private System.Windows.Forms.CheckBox optHideKnownFileExt;
        private System.Windows.Forms.CheckBox optShowKBOnly;
        private System.Windows.Forms.CheckBox optUseHighlighting;
        private System.Windows.Forms.CheckBox optAlternateEntryColor;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabPageView;
        private System.Windows.Forms.TabPage tabPageCommon;
        private System.Windows.Forms.GroupBox groupHome;
        private System.Windows.Forms.TextBox textHome;
        private System.Windows.Forms.Button buttonHome;
        private System.Windows.Forms.TabPage tabPageColors;
        private System.Windows.Forms.Button buttonFolderColor;
        private System.Windows.Forms.Label labelFolderColor;
        private System.Windows.Forms.Button buttonExecutableColor;
        private System.Windows.Forms.Label labelExecutableColor;
        private System.Windows.Forms.ColorDialog colorPicker;
    }
}