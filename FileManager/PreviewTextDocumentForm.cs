﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace FileManager
{
    public partial class PreviewTextDocumentForm : Form
    {
        public PreviewTextDocumentForm()
        {
            InitializeComponent();
            textContent.LostFocus += new EventHandler(OnLostFocusEvent);
        }

        private void OnLostFocusEvent(object sender, EventArgs e)
        {
            this.Hide();
        }

        public void PlaceUnderSelectedItem(ListView listView)
        {
            //TODO: move this to a base class!!!
            if (listView != null && listView.SelectedItems.Count == 1 && listView.SelectedIndices.Count == 1)
            {
                // Setup local position (onto list view form)
                var localPos = listView.SelectedItems[0].Position;
                localPos.Y += listView.GetItemRect(listView.SelectedIndices[0]).Height;

                // Setup global position (onto screen)
                var globalPos = listView.PointToScreen(localPos);
                this.Location = globalPos;
            }
        }

        public bool ShowFileContent(string filename)
        {
            const int maxLineCount = 35;

            var file = new StreamReader(filename);
            string line;
            int n = 0;

            Content = "";

            while ((line = file.ReadLine()) != null)
            {
                if (n < maxLineCount)
                {
                    Content += line + "\n";
                    ++n;
                }
                else
                {
                    Content += "...\n";
                    break;
                }
            }

            file.Close();

            return true;
        }

        public string Content
        {
            get
            {
                return textContent.Text;
            }
            set
            {
                textContent.Text = value;
            }
        }
    }
}
