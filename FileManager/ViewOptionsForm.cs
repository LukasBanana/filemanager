﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FileManager
{
    public partial class ViewOptionsForm : Form
    {
        private static ViewOptionsForm instance = null;

        private ViewOptions options = null;

        // Stores the view options which shall be modified
        private ViewOptionsForm()
        {
            InitializeComponent();
        }

        private void ViewOptionsForm_Load(object sender, EventArgs e)
        {
            /*var scrollBar = new VScrollBar();
            scrollBar.Dock = DockStyle.Right;
            scrollBar.Scroll += (object senderSub, ScrollEventArgs eSub) => { panelOptions.VerticalScroll.Value = scrollBar.Value; };
            panelOptions.Controls.Add(scrollBar);*/

            // Initialize user's home directory
            textHome.Text = ViewOptions.Global.HomeDir;
        }

        private void LoadViewOptions(ViewOptions viewOptions)
        {
            options = viewOptions;

            // Common settings
            textHome.Text = options.HomeDir;

            // View settings
            optShowHiddenFiles.Checked = options.ShowHiddenEntries;
            optShowExtFolders.Checked = options.ShowExtFolders;
            optUseMonthNames.Checked = options.UseMonthNames;
            optUsePrettyDays.Checked = options.UsePrettyDay;
            optShowSeconds.Checked = options.ShowSeconds;
            optShowFoldersFirst.Checked = options.ShowFoldersFirst;
            optHideUnknownFileType.Checked = options.HideUnknwonFileType;
            optHideKnownFileExt.Checked = options.HideKnownFileExt;
            optShowKBOnly.Checked = options.ShowKBOnly;
            optUseHighlighting.Checked = options.UseHighlight;
            optAlternateEntryColor.Checked = options.AlternateEntryColor;

            // Color settings
            buttonFolderColor.BackColor = options.FolderColor;
            buttonExecutableColor.BackColor = options.ExecutableColor;
        }

        private void RefreshView()
        {
            MainForm.Instance.RefreshView();
        }

        public static ViewOptionsForm ShowDialogInstance(ViewOptions options)
        {
            if (options != null)
            {
                if (instance == null)
                    instance = new ViewOptionsForm();
                instance.LoadViewOptions(options);
                return instance;
            }
            return null;
        }

        private void optShowHiddenFiles_CheckedChanged(object sender, EventArgs e)
        {
            options.ShowHiddenEntries = ((CheckBox)sender).Checked;
            RefreshView();
        }

        private void optShowExtFolders_CheckedChanged(object sender, EventArgs e)
        {
            options.ShowExtFolders = ((CheckBox)sender).Checked;
            RefreshView();
        }

        private void optUseMonthNames_CheckedChanged(object sender, EventArgs e)
        {
            options.UseMonthNames = ((CheckBox)sender).Checked;
            RefreshView();
        }

        private void optUsePrettyDays_CheckedChanged(object sender, EventArgs e)
        {
            options.UsePrettyDay = ((CheckBox)sender).Checked;
            RefreshView();
        }

        private void optShowSeconds_CheckedChanged(object sender, EventArgs e)
        {
            options.ShowSeconds = ((CheckBox)sender).Checked;
            RefreshView();
        }

        private void optShowFoldersFirst_CheckedChanged(object sender, EventArgs e)
        {
            options.ShowFoldersFirst = ((CheckBox)sender).Checked;
            RefreshView();
        }

        private void optHideUnknownFileType_CheckedChanged(object sender, EventArgs e)
        {
            options.HideUnknwonFileType = ((CheckBox)sender).Checked;
            RefreshView();
        }

        private void optHideKnownFileExt_CheckedChanged(object sender, EventArgs e)
        {
            options.HideKnownFileExt = ((CheckBox)sender).Checked;
            RefreshView();
        }

        private void optShowKBOnly_CheckedChanged(object sender, EventArgs e)
        {
            options.ShowKBOnly = ((CheckBox)sender).Checked;
            RefreshView();
        }

        private void optUseHighlighting_CheckedChanged(object sender, EventArgs e)
        {
            options.UseHighlight = ((CheckBox)sender).Checked;
            RefreshView();
        }

        private void optAlternateEntryColor_CheckedChanged(object sender, EventArgs e)
        {
            options.AlternateEntryColor = ((CheckBox)sender).Checked;
            RefreshView();
        }

        private void buttonHome_Click(object sender, EventArgs e)
        {
            var dialog = new FolderBrowserDialog();

            dialog.SelectedPath = options.HomeDir;

            if (dialog.ShowDialog() == DialogResult.OK)
                textHome.Text = options.HomeDir = dialog.SelectedPath;
        }

        private void buttonFolderColor_Click(object sender, EventArgs e)
        {
            var result = colorPicker.ShowDialog();
            if (result == DialogResult.OK)
            {
                buttonFolderColor.BackColor = options.FolderColor = colorPicker.Color;
                RefreshView();
            }
        }

        private void buttonExecutableColor_Click(object sender, EventArgs e)
        {
            var result = colorPicker.ShowDialog();
            if (result == DialogResult.OK)
            {
                buttonExecutableColor.BackColor = options.ExecutableColor = colorPicker.Color;
                RefreshView();
            }
        }
    }
}
