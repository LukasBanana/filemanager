﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace FileManager
{
    public class ViewOptions
    {
        public string HomeDir { set; get; } = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);

        public bool ShowHiddenEntries { set; get; } = false;
        public bool ShowExtFolders { set; get; } = false; // Also show "./" and "../" folders
        public bool UseMonthNames { set; get; } = true;
        public bool UsePrettyDay { set; get; } = true; // Use "Today" and "Yesterday" instead of date
        public bool ShowSeconds { set; get; } = false;
        public bool ShowFoldersFirst { set; get; } = true;
        public bool HideUnknwonFileType { set; get; } = true;
        public bool HideKnownFileExt { set; get; } = false;
        public bool ShowKBOnly { set; get; } = true; // Don't use "MB", "GB", etc.
        public bool UseHighlight { set; get; } = true;
        public bool AlternateEntryColor { set; get; } = true;

        public Color FolderColor { set; get; } = Color.Blue;
        public Color ExecutableColor { set; get; } = Color.ForestGreen;

        private static ViewOptions globalOptions = null;

        private static void AcquireGlobalOptions()
        {
            if (globalOptions == null)
                globalOptions = new ViewOptions();
        }

        public static ViewOptions Global
        {
            get
            {
                AcquireGlobalOptions();
                return globalOptions;
            }
            set
            {
                if (value != null)
                {
                    AcquireGlobalOptions();
                    globalOptions = value;
                }
            }
        }

        public ViewOptions Clone()
        {
            return (ViewOptions)this.MemberwiseClone();
        }
    }
}
