﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace FileManager
{
    class EntryAttributes
    {
        public enum Format
        {
            Short = 0, // Short info (e.g. "RW")
            Long, // Long info (e.g. "Read, Write")
        }

        public EntryAttributes()
        {
        }

        public EntryAttributes(FileInfo fileInfo)
        {
            Init(fileInfo.Attributes);
        }

        public EntryAttributes(DirectoryInfo dirInfo)
        {
            Init(dirInfo.Attributes);
        }

        private void Init(FileAttributes attribs)
        {
            ReadAccess = true;
            if (!attribs.HasFlag(FileAttributes.ReadOnly))
                WriteAccess = true;
            if (attribs.HasFlag(FileAttributes.Hidden))
                Hidden = true;
            if (attribs.HasFlag(FileAttributes.Compressed))
                Compressed = true;
            if (attribs.HasFlag(FileAttributes.Encrypted))
                Encrypted = true;
        }

        public bool ReadAccess { get; set; } = false;
        public bool WriteAccess { get; set; } = false;
        public bool Executable { get; set; } = false;
        public bool Hidden { get; set; } = false;
        public bool Compressed { get; set; } = false;
        public bool Encrypted { get; set; } = false;

        public override string ToString()
        {
            return ToString(Format.Short);
        }

        public string ToString(Format format)
        {
            var s = "";

            if (ReadAccess)
                Append(ref s, ReadAccessLabel[(int)format], format);
            if (WriteAccess)
                Append(ref s, WriteAccessLabel[(int)format], format);
            if (Executable)
                Append(ref s, ExecutableLabel[(int)format], format);
            if (Hidden)
                Append(ref s, HiddenLabel[(int)format], format);
            if (Compressed)
                Append(ref s, CompressedLabel[(int)format], format);
            if (Encrypted)
                Append(ref s, EncryptedLabel[(int)format], format);

            return s;
        }

        private void Append(ref string s, string label, Format format)
        {
            if (s.Length > 0 && format == Format.Long)
                s += ", " + label;
            else
                s += label;
        }

        private string[] ReadAccessLabel
        {
            get { return new string[] { "R", "Lesezugriff"  }; }
        }

        private string[] WriteAccessLabel
        {
            get { return new string[] { "W", "Schreibzugriff" }; }
        }

        private string[] ExecutableLabel
        {
            get { return new string[] { "X", "Ausführbar" }; }
        }

        private string[] HiddenLabel
        {
            get { return new string[] { "H", "Versteckt" }; }
        }

        private string[] CompressedLabel
        {
            get { return new string[] { "C", "Komprimiert" }; }
        }

        private string[] EncryptedLabel
        {
            get { return new string[] { "E", "Verschlüsselt" }; }
        }

    }
}
