﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FileManager
{
    public partial class MainForm : Form
    {
        private List<FolderTabUC> folderTabUCs = new List<FolderTabUC>();

        private static MainForm instance;

        public MainForm()
        {
            instance = this;
            InitializeComponent();
        }

        public static MainForm Instance
        {
            get
            {
                return instance;
            }
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
            // TESTING AREA
            OpenFolderTab(@"D:\Dokumente\Notizen");
            OpenFolderTab(@"F:\Bilder\Privat Fotos (seit 2006)\2016\Sonstiges (2016)");
            OpenFolderTab(@"D:\Entwicklung\PureBasic");

            // /TESTING AREA
        }

        private void treeViewFiles_AfterSelect(object sender, TreeViewEventArgs e)
        {

        }

        private void listViewFiles_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        public bool OpenFolderTab(string path)
        {
            try
            {
                var tab = new TabPage();
                var uc = new FolderTabUC(tab);

                uc.Folder = path;
                tab.Text = Path.GetFileName(Path.GetFullPath(path));
                tab.Controls.Add(uc);

                tabControlMain.TabPages.Add(tab);
                folderTabUCs.Add(uc);

                return true;
            }
            catch (DirectoryNotFoundException)
            {
                return false;
            }
        }

        public void RefreshView()
        {
            foreach (var tabUC in folderTabUCs)
                tabUC.RefreshView();
        }

        public FolderTabUC CurrentTab
        {
            get
            {
                var controls = tabControlMain.SelectedTab.Controls;
                return (FolderTabUC)controls[controls.Count - 1];
            }
        }

        public FolderTabUC SelectedFolderTabUC
        {
            get
            {
                return (tabControlMain.SelectedTab != null ? (FolderTabUC)tabControlMain.SelectedTab.Controls[0] : null);
            }
        }

        // Returns the status strip, if (and only if) the sender is the currently selected folder tab
        public StatusStrip RequestStatusStrip(FolderTabUC sender)
        {
            return (sender == SelectedFolderTabUC ? statusStripMain : null);
        }

        // Shows the content of the specified folder in the current selected tab
        public void ShowFolderContent(string path)
        {
            SelectedFolderTabUC.Folder = path;
        }

        private void tabControlMain_SelectedIndexChanged(object sender, EventArgs e)
        {
            SelectedFolderTabUC.SetupStatusStrip(statusStripMain);
        }
    }
}
