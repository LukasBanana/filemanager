﻿namespace FileManager
{
    partial class FolderTabUC
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FolderTabUC));
            this.splitContainerMain = new System.Windows.Forms.SplitContainer();
            this.treeViewFiles = new FileManager.FolderTreeView();
            this.listViewFiles = new FileManager.FolderListView();
            this.columnName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnDateModified = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnType = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnSize = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnAttributes = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.contextMenuFile = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolMenuOpen = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.toolMenuCut = new System.Windows.Forms.ToolStripMenuItem();
            this.toolMenuCopy = new System.Windows.Forms.ToolStripMenuItem();
            this.toolMenuPaste = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolMenuDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.toolMenuRename = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.toolMenuProperties = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMain = new FileManager.FolderToolStrip();
            this.toolButtonBack = new System.Windows.Forms.ToolStripButton();
            this.toolButtonForward = new System.Windows.Forms.ToolStripButton();
            this.toolButtonUpward = new System.Windows.Forms.ToolStripButton();
            this.toolButtonHome = new System.Windows.Forms.ToolStripButton();
            this.toolButtonSettings = new System.Windows.Forms.ToolStripButton();
            this.toolButtonView = new System.Windows.Forms.ToolStripDropDownButton();
            this.toolViewItemDetails = new System.Windows.Forms.ToolStripMenuItem();
            this.toolViewItemList = new System.Windows.Forms.ToolStripMenuItem();
            this.toolViewItemSmallIcons = new System.Windows.Forms.ToolStripMenuItem();
            this.toolViewItemLargeIcons = new System.Windows.Forms.ToolStripMenuItem();
            this.toolViewItemTiles = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolPathBox = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolSearchBox = new System.Windows.Forms.ToolStripComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerMain)).BeginInit();
            this.splitContainerMain.Panel1.SuspendLayout();
            this.splitContainerMain.Panel2.SuspendLayout();
            this.splitContainerMain.SuspendLayout();
            this.contextMenuFile.SuspendLayout();
            this.toolStripMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainerMain
            // 
            resources.ApplyResources(this.splitContainerMain, "splitContainerMain");
            this.splitContainerMain.Name = "splitContainerMain";
            // 
            // splitContainerMain.Panel1
            // 
            resources.ApplyResources(this.splitContainerMain.Panel1, "splitContainerMain.Panel1");
            this.splitContainerMain.Panel1.Controls.Add(this.treeViewFiles);
            // 
            // splitContainerMain.Panel2
            // 
            resources.ApplyResources(this.splitContainerMain.Panel2, "splitContainerMain.Panel2");
            this.splitContainerMain.Panel2.Controls.Add(this.listViewFiles);
            // 
            // treeViewFiles
            // 
            resources.ApplyResources(this.treeViewFiles, "treeViewFiles");
            this.treeViewFiles.FullRowSelect = true;
            this.treeViewFiles.HideSelection = false;
            this.treeViewFiles.Name = "treeViewFiles";
            this.treeViewFiles.ShowLines = false;
            // 
            // listViewFiles
            // 
            resources.ApplyResources(this.listViewFiles, "listViewFiles");
            this.listViewFiles.AllowColumnReorder = true;
            this.listViewFiles.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnName,
            this.columnDateModified,
            this.columnType,
            this.columnSize,
            this.columnAttributes});
            this.listViewFiles.FullRowSelect = true;
            this.listViewFiles.HideSelection = false;
            this.listViewFiles.Name = "listViewFiles";
            this.listViewFiles.UseCompatibleStateImageBehavior = false;
            this.listViewFiles.View = System.Windows.Forms.View.Details;
            this.listViewFiles.SelectedIndexChanged += new System.EventHandler(this.listViewFiles_SelectedIndexChanged);
            this.listViewFiles.MouseClick += new System.Windows.Forms.MouseEventHandler(this.listViewFiles_MouseClick);
            this.listViewFiles.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listViewFiles_MouseDoubleClick);
            // 
            // columnName
            // 
            resources.ApplyResources(this.columnName, "columnName");
            // 
            // columnDateModified
            // 
            resources.ApplyResources(this.columnDateModified, "columnDateModified");
            // 
            // columnType
            // 
            resources.ApplyResources(this.columnType, "columnType");
            // 
            // columnSize
            // 
            resources.ApplyResources(this.columnSize, "columnSize");
            // 
            // columnAttributes
            // 
            resources.ApplyResources(this.columnAttributes, "columnAttributes");
            // 
            // contextMenuFile
            // 
            resources.ApplyResources(this.contextMenuFile, "contextMenuFile");
            this.contextMenuFile.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolMenuOpen,
            this.toolStripSeparator4,
            this.toolMenuCut,
            this.toolMenuCopy,
            this.toolMenuPaste,
            this.toolStripSeparator2,
            this.toolMenuDelete,
            this.toolMenuRename,
            this.toolStripSeparator5,
            this.toolMenuProperties});
            this.contextMenuFile.Name = "contextMenuStrip1";
            // 
            // toolMenuOpen
            // 
            resources.ApplyResources(this.toolMenuOpen, "toolMenuOpen");
            this.toolMenuOpen.Name = "toolMenuOpen";
            // 
            // toolStripSeparator4
            // 
            resources.ApplyResources(this.toolStripSeparator4, "toolStripSeparator4");
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            // 
            // toolMenuCut
            // 
            resources.ApplyResources(this.toolMenuCut, "toolMenuCut");
            this.toolMenuCut.Name = "toolMenuCut";
            // 
            // toolMenuCopy
            // 
            resources.ApplyResources(this.toolMenuCopy, "toolMenuCopy");
            this.toolMenuCopy.Name = "toolMenuCopy";
            // 
            // toolMenuPaste
            // 
            resources.ApplyResources(this.toolMenuPaste, "toolMenuPaste");
            this.toolMenuPaste.Name = "toolMenuPaste";
            // 
            // toolStripSeparator2
            // 
            resources.ApplyResources(this.toolStripSeparator2, "toolStripSeparator2");
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            // 
            // toolMenuDelete
            // 
            resources.ApplyResources(this.toolMenuDelete, "toolMenuDelete");
            this.toolMenuDelete.Name = "toolMenuDelete";
            // 
            // toolMenuRename
            // 
            resources.ApplyResources(this.toolMenuRename, "toolMenuRename");
            this.toolMenuRename.Name = "toolMenuRename";
            // 
            // toolStripSeparator5
            // 
            resources.ApplyResources(this.toolStripSeparator5, "toolStripSeparator5");
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            // 
            // toolMenuProperties
            // 
            resources.ApplyResources(this.toolMenuProperties, "toolMenuProperties");
            this.toolMenuProperties.Name = "toolMenuProperties";
            this.toolMenuProperties.Click += new System.EventHandler(this.toolMenuProperties_Click);
            // 
            // toolStripMain
            // 
            resources.ApplyResources(this.toolStripMain, "toolStripMain");
            this.toolStripMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolButtonBack,
            this.toolButtonForward,
            this.toolButtonUpward,
            this.toolButtonHome,
            this.toolButtonSettings,
            this.toolButtonView,
            this.toolStripSeparator1,
            this.toolPathBox,
            this.toolStripSeparator3,
            this.toolSearchBox});
            this.toolStripMain.Name = "toolStripMain";
            this.toolStripMain.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.toolStripMain_ItemClicked);
            // 
            // toolButtonBack
            // 
            resources.ApplyResources(this.toolButtonBack, "toolButtonBack");
            this.toolButtonBack.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolButtonBack.Name = "toolButtonBack";
            // 
            // toolButtonForward
            // 
            resources.ApplyResources(this.toolButtonForward, "toolButtonForward");
            this.toolButtonForward.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolButtonForward.Name = "toolButtonForward";
            // 
            // toolButtonUpward
            // 
            resources.ApplyResources(this.toolButtonUpward, "toolButtonUpward");
            this.toolButtonUpward.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolButtonUpward.Name = "toolButtonUpward";
            this.toolButtonUpward.Click += new System.EventHandler(this.toolButtonUpward_Click);
            // 
            // toolButtonHome
            // 
            resources.ApplyResources(this.toolButtonHome, "toolButtonHome");
            this.toolButtonHome.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolButtonHome.Name = "toolButtonHome";
            this.toolButtonHome.Click += new System.EventHandler(this.toolButtonHome_Click);
            // 
            // toolButtonSettings
            // 
            resources.ApplyResources(this.toolButtonSettings, "toolButtonSettings");
            this.toolButtonSettings.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolButtonSettings.Name = "toolButtonSettings";
            this.toolButtonSettings.Click += new System.EventHandler(this.toolButtonSettings_Click);
            // 
            // toolButtonView
            // 
            resources.ApplyResources(this.toolButtonView, "toolButtonView");
            this.toolButtonView.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolButtonView.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolViewItemDetails,
            this.toolViewItemList,
            this.toolViewItemSmallIcons,
            this.toolViewItemLargeIcons,
            this.toolViewItemTiles});
            this.toolButtonView.Name = "toolButtonView";
            // 
            // toolViewItemDetails
            // 
            resources.ApplyResources(this.toolViewItemDetails, "toolViewItemDetails");
            this.toolViewItemDetails.Checked = true;
            this.toolViewItemDetails.CheckState = System.Windows.Forms.CheckState.Indeterminate;
            this.toolViewItemDetails.Name = "toolViewItemDetails";
            this.toolViewItemDetails.Click += new System.EventHandler(this.toolViewItemDetails_Click);
            // 
            // toolViewItemList
            // 
            resources.ApplyResources(this.toolViewItemList, "toolViewItemList");
            this.toolViewItemList.Name = "toolViewItemList";
            this.toolViewItemList.Click += new System.EventHandler(this.toolViewItemList_Click);
            // 
            // toolViewItemSmallIcons
            // 
            resources.ApplyResources(this.toolViewItemSmallIcons, "toolViewItemSmallIcons");
            this.toolViewItemSmallIcons.Name = "toolViewItemSmallIcons";
            this.toolViewItemSmallIcons.Click += new System.EventHandler(this.toolViewItemSmallIcons_Click);
            // 
            // toolViewItemLargeIcons
            // 
            resources.ApplyResources(this.toolViewItemLargeIcons, "toolViewItemLargeIcons");
            this.toolViewItemLargeIcons.Name = "toolViewItemLargeIcons";
            this.toolViewItemLargeIcons.Click += new System.EventHandler(this.toolViewItemLargeIcons_Click);
            // 
            // toolViewItemTiles
            // 
            resources.ApplyResources(this.toolViewItemTiles, "toolViewItemTiles");
            this.toolViewItemTiles.Name = "toolViewItemTiles";
            this.toolViewItemTiles.Click += new System.EventHandler(this.toolViewItemTiles_Click);
            // 
            // toolStripSeparator1
            // 
            resources.ApplyResources(this.toolStripSeparator1, "toolStripSeparator1");
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            // 
            // toolPathBox
            // 
            this.toolPathBox.AcceptsReturn = true;
            resources.ApplyResources(this.toolPathBox, "toolPathBox");
            this.toolPathBox.Name = "toolPathBox";
            this.toolPathBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.toolPathBox_KeyDown);
            // 
            // toolStripSeparator3
            // 
            resources.ApplyResources(this.toolStripSeparator3, "toolStripSeparator3");
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            // 
            // toolSearchBox
            // 
            resources.ApplyResources(this.toolSearchBox, "toolSearchBox");
            this.toolSearchBox.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolSearchBox.Name = "toolSearchBox";
            // 
            // FolderTabUC
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainerMain);
            this.Controls.Add(this.toolStripMain);
            this.Name = "FolderTabUC";
            this.Load += new System.EventHandler(this.FolderTabUC_Load);
            this.splitContainerMain.Panel1.ResumeLayout(false);
            this.splitContainerMain.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerMain)).EndInit();
            this.splitContainerMain.ResumeLayout(false);
            this.contextMenuFile.ResumeLayout(false);
            this.toolStripMain.ResumeLayout(false);
            this.toolStripMain.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private FileManager.FolderToolStrip toolStripMain;
        private System.Windows.Forms.ToolStripButton toolButtonBack;
        private System.Windows.Forms.ToolStripButton toolButtonForward;
        private System.Windows.Forms.ToolStripButton toolButtonUpward;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripTextBox toolPathBox;
        private System.Windows.Forms.ToolStripComboBox toolSearchBox;
        private System.Windows.Forms.SplitContainer splitContainerMain;
        private FileManager.FolderTreeView treeViewFiles;
        private FileManager.FolderListView listViewFiles;
        private System.Windows.Forms.ColumnHeader columnName;
        private System.Windows.Forms.ColumnHeader columnDateModified;
        private System.Windows.Forms.ColumnHeader columnType;
        private System.Windows.Forms.ColumnHeader columnSize;
        private System.Windows.Forms.ColumnHeader columnAttributes;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton toolButtonHome;
        private System.Windows.Forms.ToolStripButton toolButtonSettings;
        private System.Windows.Forms.ContextMenuStrip contextMenuFile;
        private System.Windows.Forms.ToolStripMenuItem toolMenuOpen;
        private System.Windows.Forms.ToolStripMenuItem toolMenuProperties;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem toolMenuCut;
        private System.Windows.Forms.ToolStripMenuItem toolMenuCopy;
        private System.Windows.Forms.ToolStripMenuItem toolMenuPaste;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem toolMenuRename;
        private System.Windows.Forms.ToolStripMenuItem toolMenuDelete;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripDropDownButton toolButtonView;
        private System.Windows.Forms.ToolStripMenuItem toolViewItemDetails;
        private System.Windows.Forms.ToolStripMenuItem toolViewItemTiles;
        private System.Windows.Forms.ToolStripMenuItem toolViewItemList;
        private System.Windows.Forms.ToolStripMenuItem toolViewItemSmallIcons;
        private System.Windows.Forms.ToolStripMenuItem toolViewItemLargeIcons;
    }
}
