﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace FileManager
{
    class FileTypeDenoter
    {

        private static FileTypeDenoter instance = null;

        private List<FileFormatAssociation> fileFormatAssociations = new List<FileFormatAssociation>();

        private FileTypeDenoter()
        {
            ResetFileFormatAssociations();
        }

        public static FileTypeDenoter Instance
        {
            get
            {
                if (instance == null)
                    instance = new FileTypeDenoter();
                return instance;
            }
        }

        private void RegisterFormat(string typeName, uint magicNumber, string ext = "", string program = "", bool executable = false)
        {
            fileFormatAssociations.Add(new FileFormatAssociation(typeName, magicNumber, ext, program, executable));
        }

        private void RegisterFormat(string typeName, string ext, string program = "", bool executable = false)
        {
            fileFormatAssociations.Add(new FileFormatAssociation(typeName, ext, program, executable));
        }

        public void ResetFileFormatAssociations()
        {
            // Register by magic numbers
            RegisterFormat("Programm", 0x5a4d, "", "?", true);
            RegisterFormat("Programmerweiterung", 0x5a4d, "DLL");
            RegisterFormat("Systemerweiterung", 0x5a4d, "SYS");
            RegisterFormat("Bitmap Grafik", 0x4d42);
            RegisterFormat("GZIP Kompressionsdatei", 0x8b1f);
            RegisterFormat("Bitmap Grafik", 0x4d42);

            RegisterFormat("MP3 Musik", 0x494433);
            RegisterFormat("JPEG Grafik", 0xffd8ff);

            RegisterFormat("Programm (Unix)", 0x464c457f);
            RegisterFormat("Programmerweiterung (Unix)", 0x464c457f, "SO");
            RegisterFormat("MIDI Musik", 0x6468544d);
            RegisterFormat("FLAC Musik", 0x43614C66);
            RegisterFormat("Matroska Video", 0xa3df451a, "MKV", @"D:\Anwendungen\VLC\vlc.exe");
            RegisterFormat("Wave Sound", 0x46464952, "WAV", @"D:\Anwendungen\VLC\vlc.exe");
            RegisterFormat("AVI Video", 0x46464952, "AVI", @"D:\Anwendungen\VLC\vlc.exe");
            RegisterFormat("AVI Video", 0x46464952, "AVI", @"D:\Anwendungen\VLC\vlc.exe");
            RegisterFormat("PNG Grafik", 0x474e5089);
            RegisterFormat("Verknüpfung", 0x0000004c);

            // Register by file extensions
            RegisterFormat("C# Source Code", "CS");
            RegisterFormat("C Source Code", "C");
            RegisterFormat("C++ Source Code", "CPP");
            RegisterFormat("C Header", "H");
            RegisterFormat("C++ Header", "HPP");
            RegisterFormat("Objective-C Source Code", "M");
            RegisterFormat("Textdokument", "TXT", @"D:\Anwendungen\Notepad++\notepad++.exe");
        }

        private ushort ReadMagic2(string filename)
        {
            using (var stream = File.OpenRead(filename))
            {
                var reader = new BinaryReader(stream);
                return reader.ReadUInt16();
            }
        }

        private uint ReadMagic3(string filename)
        {
            using (var stream = File.OpenRead(filename))
            {
                uint magic = 0;
                magic = (uint)stream.ReadByte(); magic <<= 8;
                magic += (uint)stream.ReadByte(); magic <<= 8;
                magic += (uint)stream.ReadByte();
                return magic;
            }
        }

        private uint ReadMagic4(string filename)
        {
            using (var stream = File.OpenRead(filename))
            {
                var reader = new BinaryReader(stream);
                return reader.ReadUInt32();
            }
        }

        public FileFormatAssociation FindFileFormatAssociation(string filename)
        {
            // Get file extension
            var ext = Path.GetExtension(filename).ToUpper();
            if (ext.Length > 0 && ext.First() == '.')
                ext = ext.Substring(1);

            // Try to determine file type by its magic number
            try
            {
                var magicNo2 = ReadMagic2(filename);
                var magicNo3 = ReadMagic3(filename);
                var magicNo4 = ReadMagic4(filename);

                // Collect all associations with valid magic numbers (some magic numbers might be used several times)
                var typeList = new List<FileFormatAssociation>();

                foreach (var assoc in fileFormatAssociations)
                {
                    if (assoc.MagicMatch(magicNo2, magicNo3, magicNo4))
                        typeList.Add(assoc);
                    else if (typeList.Count > 0)
                        break;
                }

                // If there are associations with several equal magic numbers => use extension to determine type
                if (typeList.Count == 1)
                    return typeList[0];
                else if (typeList.Count > 1)
                {
                    FileFormatAssociation assocFinal = null;

                    foreach (var assoc in typeList)
                    {
                        if (assocFinal == null)
                            assocFinal = assoc;
                        else if (assoc.ext.Length == 0)
                            assocFinal = assoc;
                        if (ext == assoc.ext.ToUpper())
                            return assoc;
                    }

                    return assocFinal;
                }

                // Try to determine file type by its content (partially)
                /*if (magicNo2 == 0x2123)
                {
                    using (var stream = new StreamReader(filename))
                    {
                        var line = stream.ReadLine();
                        if (line == "#!/bin/bash" || line == "#!/bin/sh")
                            return new FileFormatAssociation("Bash Shell Skript", "SH", "PowerShell.exe");//!!!
                    }
                }*/
            }
            catch (UnauthorizedAccessException)
            {
                // Ignore reading exceptions here
            }
            catch (EndOfStreamException)
            {
                // Ignore reading exceptions here
            }
            catch (IOException)
            {
                // Ignore reading exceptions here
            }

            // Try to determine file type by its extension
            if (ext.Length != 0)
            {
                foreach (var assoc in fileFormatAssociations)
                {
                    if (assoc.ext == ext)
                        return assoc;
                }
            }

            return null;
        }

    }
}
