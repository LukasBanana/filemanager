﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileManager
{
    class FileFormatAssociation
    {

        public string typeName = ""; // File format type name (e.g. "Bitmap Graphics").
        public uint magicNumber = 0; // Magic number (2, 3, or 4 bytes used depending on how large the number is)
        public string ext = ""; // File extension or empty if unused
        public string program = ""; // Program associated to this file format.
        public bool executable = false; // File is an executable file

        public FileFormatAssociation(string typeName, uint magicNumber, string ext = "", string program = "", bool executable = false)
        {
            this.magicNumber = magicNumber;
            this.typeName = typeName;
            this.ext = ext;
            this.program = program;
            this.executable = executable;
        }

        public FileFormatAssociation(string typeName, string ext, string program = "", bool executable = false)
        {
            this.typeName = typeName;
            this.ext = ext;
            this.program = program;
            this.executable = executable;
        }

        // Returns true if the specified magic number matches the number in this file format association.
        public bool MagicMatch(uint magic2, uint magic3, uint magic4)
        {
            return (magicNumber != 0 && (magic2 == magicNumber || magic3 == magicNumber || magic4 == magicNumber));
        }

        public string Hex
        {
            get
            {
                return magicNumber.ToString("X");
            }
        }

        // Returns true if this file format association has a magic number consisting of 2 bytes.
        public bool HasMagic2
        {
            get
            {
                return magicNumber != 0 && magicNumber <= 0x0000ffff;
            }
        }

        // Returns true if this file format association has a magic number consisting of 3 bytes.
        public bool HasMagic3
        {
            get
            {
                return magicNumber != 0 && magicNumber > 0x0000ffff && magicNumber <= 0x00ffffff;
            }
        }

        // Returns true if this file format association has a magic number consisting of 4 bytes.
        public bool HasMagic4
        {
            get
            {
                return magicNumber != 0 && magicNumber > 0x00ffffff;
            }
        }

    }
}
