﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;

namespace FileManager
{
    public partial class FolderTabUC : UserControl
    {
        private TabPage tabPageParent = null;
        private FileSystemWatcher fileWatcher = new FileSystemWatcher();
        private PreviewTextDocumentForm previewTextDocForm = new PreviewTextDocumentForm(); //TODO: maybe use base class "PreviewForm"
        private PreviewPictureForm previewPictureForm = new PreviewPictureForm();

        public FolderTabUC(TabPage tabPageParent)
        {
            InitializeComponent();

            this.Dock = DockStyle.Fill;
            this.tabPageParent = tabPageParent;

            // Initialize file system watcher
            fileWatcher.NotifyFilter = NotifyFilters.FileName | NotifyFilters.DirectoryName | NotifyFilters.LastWrite | NotifyFilters.Security;

            fileWatcher.Changed += new FileSystemEventHandler(OnFileChanged);
            fileWatcher.Renamed += new RenamedEventHandler(OnFileRenamed);

            // Setup key event handler
            listViewFiles.KeyDown += new KeyEventHandler(OnListViewKeyDown);
            listViewFiles.MouseWheel += new MouseEventHandler(OnMouseWheel);
        }

        private void FolderTabUC_Load(object sender, EventArgs e)
        {
            treeViewFiles.ResetRootNodes();
        }

        private void OnMouseWheel(object sender, MouseEventArgs e)
        {
            int value = Math.Sign(e.Delta);

            if (Control.ModifierKeys == Keys.Alt)
            {
                if (value > 0)
                    SelectNextListView();
                else
                    SelectPreviousListView();
            }
            else if (Control.ModifierKeys == Keys.Control)
                listViewFiles.Zoom((float)value);
        }

        private void OnListViewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control)
            {
                switch (e.KeyCode)
                {
                    case Keys.Oemplus:
                    case Keys.Add:
                        listViewFiles.ZoomIn();
                        break;

                    case Keys.OemMinus:
                    case Keys.Subtract:
                        listViewFiles.ZoomOut();
                        break;

                    case Keys.D0:
                    case Keys.NumPad0:
                        listViewFiles.ZoomReset();
                        break;
                }
            }
            else
            {
                switch (e.KeyCode)
                {
                    case Keys.Space:
                        ShowSelectedFileContent();
                        break;
                }
            }
        }

        // Returns the full path of all selected filenames (also directories)
        public string[] SelectedFilenames
        {
            get
            {
                int n = listViewFiles.SelectedItems.Count;
                var files = new string[n];

                for (int i = 0; i < n; ++i)
                    files[i] = Folder + "\\" + listViewFiles.SelectedItems[i].SubItems[0].Text;

                return files;
            }
        }

        private void ShowSelectedFileContent()
        {
            if (listViewFiles.SelectedItems.Count != 1)
                return;

            // Get file information
            string file = SelectedFilenames[0];
            var type = FileTypeDenoter.Instance.FindFileFormatAssociation(file);

            if (type != null)
            {
                //TODO: clean this up!!!
                var ext = Path.GetExtension(file).ToUpper();
                if (ext.Length > 0 && ext[0] == '.')
                    ext = ext.Substring(1);

                if (ext == "TXT" || ext == "C" || ext == "CPP" || ext == "H" || ext == "HPP" || ext == "CS" || ext == "M")
                {
                    previewTextDocForm.ShowFileContent(file);
                    previewTextDocForm.Show(this);
                    previewTextDocForm.PlaceUnderSelectedItem(listViewFiles);
                }
                else if (ext == "JPG" || ext == "JPEG" || ext == "PNG" || ext == "BMP")
                {
                    previewPictureForm.ShowFileContent(file);
                    previewPictureForm.Show(this);
                    previewPictureForm.PlaceUnderSelectedItem(listViewFiles);
                }
            }
        }

        private void SetupFileWatcher(string path)
        {
            //fileWatcher.Path = path;
            //fileWatcher.EnableRaisingEvents = true;
        }

        private void OnFileChanged(object sender, FileSystemEventArgs e)
        {
            //TODO: only update specific entry
            //listViewFiles.ShowFolderContent(folderPath);
        }

        private void OnFileRenamed(object sender, RenamedEventArgs e)
        {
            //TODO: only update specific entry
            //WARNING: Threading problem here!!!
            //listViewFiles.ShowFolderContent(folderPath);
        }

        private void listViewFiles_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void listViewFiles_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                for (int i = 0; i < listViewFiles.SelectedItems.Count; ++i)
                {
                    // Get filename or path
                    var file = toolPathBox.Text + "\\" + listViewFiles.SelectedItems[i].Text;

                    if (Directory.Exists(file))
                    {
                        // Open new folder
                        Folder = file;
                    }
                    else
                    {
                        // Run program for file
                        var type = FileTypeDenoter.Instance.FindFileFormatAssociation(file);

                        if (type != null && type.program.Length > 0)
                        {
                            var process = new ProcessStartInfo();

                            if (type.program == "?")
                                process.FileName = file;
                            else
                            {
                                process.FileName = type.program;
                                process.Arguments = "\"" + file + "\"";
                            }

                            Process.Start(process);
                        }
                    }
                }
            }
        }

        private void listViewFiles_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                contextMenuFile.Show(listViewFiles.PointToScreen(e.Location));
            }
        }

        private string folderPath = "";

        private FolderListView.FolderInfo folderInfo = null;

        public string Folder
        {
            get
            {
                return folderPath;
            }
            set
            {
                if (!String.IsNullOrEmpty(value))
                {
                    // Adjust path
                    folderPath = Path.GetFullPath(value);
                    SetupFileWatcher(folderPath);

                    // Show new folder content
                    folderInfo = listViewFiles.ShowFolderContent(folderPath);

                    // Set path in text box and tab panel
                    toolPathBox.Text = folderPath;
                    tabPageParent.Text = Path.GetFileName(folderPath);

                    // Set status bar information
                    SetupStatusStrip(MainForm.Instance.RequestStatusStrip(this));
                }
            }
        }

        public void SetupStatusStrip(StatusStrip statusStrip)
        {
            if (statusStrip != null)
            {
                var s = "";

                if (folderInfo != null)
                {
                    var numSubFolders = $"{folderInfo.NumSubFolders} Ordner";
                    var numFiles = $"{folderInfo.NumFiles} {(folderInfo.NumFiles == 1 ? "Datei" : "Dateien")}";
                    var owner = Directory.GetAccessControl(Folder).GetOwner(typeof(System.Security.Principal.NTAccount)).ToString();

                    s = $"{numSubFolders}, {numFiles}, Besitzer: {owner}";
                }

                statusStrip.Items[0].Text = s;
            }
        }

        public void RefreshView()
        {
            //TODO: store scroll position
            listViewFiles.ShowFolderContent(Folder);
            //TODO: restore scroll position
        }

        private void toolStripMain_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void toolButtonUpward_Click(object sender, EventArgs e)
        {
            var parent = Directory.GetParent(Folder);
            if (parent != null)
                Folder = parent.FullName;
        }

        private void toolPathBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                var prevPath = Folder;

                try
                {
                    Folder = toolPathBox.Text;
                }
                catch (DirectoryNotFoundException)
                {
                    MessageBox.Show(
                        caption: "Ungültiges Verzeichnis",
                        text: $"Das Verzeichnis \"{toolPathBox.Text}\" konnte nicht gefunden werden!",
                        buttons: MessageBoxButtons.OK, icon: MessageBoxIcon.Error, owner: this
                    );
                    Folder = prevPath;
                }
            }
        }

        private void toolButtonSettings_Click(object sender, EventArgs e)
        {
            // Store previous view options and open view options dialog box
            var prevOptions = ViewOptions.Global.Clone();
            var dialog = ViewOptionsForm.ShowDialogInstance(ViewOptions.Global);

            // If canceled -> restore previous view options
            if (dialog.ShowDialog(this) == DialogResult.Cancel)
            {
                ViewOptions.Global = prevOptions;
                MainForm.Instance.RefreshView();
            }
        }

        private void toolButtonHome_Click(object sender, EventArgs e)
        {
            if (ViewOptions.Global.HomeDir != "")
                Folder = ViewOptions.Global.HomeDir;
        }

        private void toolMenuProperties_Click(object sender, EventArgs e)
        {
            var count = listViewFiles.SelectedItems.Count;

            if (count > 1)
            {
                var answer = MessageBox.Show(
                    caption: "Mehrere Dateien selektiert",
                    text: $"Wollen Sie wirklich die Eigenschaften von allen {count} Elementen anzeigen?",
                    buttons: MessageBoxButtons.OKCancel,
                    defaultButton: MessageBoxDefaultButton.Button2,
                    icon: MessageBoxIcon.Warning,
                    owner: this
                );
                if (answer == DialogResult.Cancel)
                    return;
            }

            for (int i = 0; i < count; ++i)
            {
                // Get filename or path
                var file = toolPathBox.Text + "\\" + listViewFiles.SelectedItems[i].Text;

                if (File.Exists(file))
                    FilePropertyDialog.Show(file);
            }
        }

        private ToolStripMenuItem[] ViewRadioCheckItems
        {
            get
            {
                return new ToolStripMenuItem[]
                {
                    toolViewItemDetails,
                    toolViewItemList,
                    toolViewItemSmallIcons,
                    toolViewItemLargeIcons,
                    toolViewItemTiles,
                };
            }
        }

        private View[] ViewChoices
        {
            get
            {
                return new View[]
                {
                    View.Details,
                    View.List,
                    View.SmallIcon,
                    View.LargeIcon,
                    View.Tile
                };
            }
        }

        private ToolStripMenuItem ViewRadioCheck
        {
            set
            {
                foreach (var item in ViewRadioCheckItems)
                {
                    item.Checked = (value == item);
                    if (item.Checked)
                        item.CheckState = CheckState.Indeterminate;
                }
            }
        }

        private void SelectNextListView()
        {
            var items = ViewRadioCheckItems;
            var choices = ViewChoices;

            int i = 0, n = items.Length;

            foreach (var item in ViewRadioCheckItems)
            {
                if (item.Checked)
                {
                    if (i + 1 < n)
                    {
                        ViewRadioCheck = items[i + 1];
                        listViewFiles.View = choices[i + 1];
                    }
                    else
                    {
                        ViewRadioCheck = items[0];
                        listViewFiles.View = choices[0];
                    }
                    break;
                }
                ++i;
            }
        }

        private void SelectPreviousListView()
        {
            var items = ViewRadioCheckItems;
            var choices = ViewChoices;

            int i = 0, n = items.Length;

            foreach (var item in ViewRadioCheckItems)
            {
                if (item.Checked)
                {
                    if (i > 0)
                    {
                        ViewRadioCheck = items[i - 1];
                        listViewFiles.View = choices[i - 1];
                    }
                    else
                    {
                        ViewRadioCheck = items[n - 1];
                        listViewFiles.View = choices[n - 1];
                    }
                    break;
                }
                ++i;
            }
        }

        private void toolViewItemDetails_Click(object sender, EventArgs e)
        {
            ViewRadioCheck = (ToolStripMenuItem)sender;
            listViewFiles.View = View.Details;
        }

        private void toolViewItemList_Click(object sender, EventArgs e)
        {
            ViewRadioCheck = (ToolStripMenuItem)sender;
            listViewFiles.View = View.List;
        }

        private void toolViewItemSmallIcons_Click(object sender, EventArgs e)
        {
            ViewRadioCheck = (ToolStripMenuItem)sender;
            listViewFiles.View = View.SmallIcon;
        }

        private void toolViewItemLargeIcons_Click(object sender, EventArgs e)
        {
            ViewRadioCheck = (ToolStripMenuItem)sender;
            listViewFiles.View = View.LargeIcon;
        }

        private void toolViewItemTiles_Click(object sender, EventArgs e)
        {
            ViewRadioCheck = (ToolStripMenuItem)sender;
            listViewFiles.View = View.Tile;
        }
    }
}
