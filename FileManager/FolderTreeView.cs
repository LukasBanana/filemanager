﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Drawing;
using System.Security.Principal;
using System.DirectoryServices;

namespace FileManager
{
    class FolderTreeView : TreeView
    {
        Dictionary<TreeNode, string> parentPaths = new Dictionary<TreeNode, string>();

        public FolderTreeView()
        {
            this.AfterExpand += new TreeViewEventHandler(this.NodeExpandEvent);
            this.AfterSelect += new TreeViewEventHandler(this.NodeSelectEvent);
        }

        public void ResetRootNodes()
        {
            Nodes.Clear();
            AppendQuickEntries();
            AppendLocalDrives();
            AppendNetworkDevices();
        }

        private void AppendQuickEntries()
        {
            var rootNode = Nodes.Add("Schnellzugriff");

            // Add user name directory (C:\Users\<NAME>)
            var userName = WindowsIdentity.GetCurrent().Name;

            AppendDirectory(rootNode, userName, Environment.GetFolderPath(Environment.SpecialFolder.UserProfile));
            AppendDirectory(rootNode, "Desktop", Environment.GetFolderPath(Environment.SpecialFolder.Desktop));
            AppendDirectory(rootNode, "Downloads", Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + "\\Downloads");
            AppendDirectory(rootNode, "Favoriten", Environment.GetFolderPath(Environment.SpecialFolder.Favorites));
            AppendDirectory(rootNode, "Letzen Zugriffe", Environment.GetFolderPath(Environment.SpecialFolder.Recent));

            rootNode.Expand();
        }

        // Appends the nodes for all local drives (e.g. "Windows (C:)")
        private void AppendLocalDrives()
        {
            var rootNode = Nodes.Add("Lokaler PC");

            if (Nodes.Count != 0)
            {
                var drives = DriveInfo.GetDrives();

                foreach (var drive in drives)
                {
                    // Setup drive label
                    var driveLabel = $"({drive.Name})";

                    if (drive.IsReady)
                        driveLabel = $"{drive.VolumeLabel} {driveLabel} {drive.DriveFormat}";
                    else
                        driveLabel = $"[{drive.DriveType}] {driveLabel}";

                    // Add tree node for drive
                    var node = rootNode.Nodes.Add(driveLabel);

                    if (drive.IsReady)
                    {
                        // Append first sub directories
                        AppendSubDirectories(node, drive.Name);
                    }
                    else
                        node.ForeColor = Color.Gray;
                }
            }

            rootNode.Expand();
        }

        private void AppendNetworkDevices()
        {
            var rootNode = Nodes.Add("Netzwerk");

            //rootNode.Expand();
        }

        private void AppendDirectory(TreeNode rootNode, string label, string path)
        {
            var node = rootNode.Nodes.Add(label);
            parentPaths[node] = path;
        }

        private void AppendSubDirectories(TreeNode parent, string path)
        {
            var options = ViewOptions.Global;

            // Only append sub directory once for a parent node
            if (parent != null && !parentPaths.ContainsKey(parent))
            {
                parentPaths.Add(parent, path);

                try
                {
                    // Append all sub directories within this parent directory node
                    var dirs = new List<string>(Directory.EnumerateDirectories(path));
                    foreach (var dir in dirs)
                    {
                        // Get directory information
                        var info = new DirectoryInfo(dir);

                        if (options.ShowHiddenEntries || !info.Attributes.HasFlag(FileAttributes.Hidden))
                        {
                            // Append new directory node
                            var dirName = Path.GetFileName(dir);
                            parent.Nodes.Add(dirName);
                        }
                    }
                }
                catch (UnauthorizedAccessException)
                {
                    // Ignore expection here
                }
            }
        }

        private string GetFullChildNodePath(TreeNode node, string parentPath)
        {
            if (!parentPath.EndsWith("\\") && !parentPath.EndsWith("/"))
                parentPath += "\\";

            var path = parentPath + node.Text;

            return path;
        }

        public void NodeExpandEvent(object sender, TreeViewEventArgs e)
        {
            var node = e.Node;

            if (parentPaths.ContainsKey(node))
            {
                var path = parentPaths[node];
                for (int i = 0; i < node.Nodes.Count; ++i)
                {
                    var child = node.Nodes[i];
                    AppendSubDirectories(child, GetFullChildNodePath(child, path));
                }
            }
        }

        public void NodeSelectEvent(object sender, TreeViewEventArgs e)
        {
            var node = e.Node;

            // Get selected path
            string path = "";
            if (parentPaths.ContainsKey(node))
                path = parentPaths[node];
            else if (node.Parent != null && parentPaths.ContainsKey(node.Parent))
                path = GetFullChildNodePath(node, parentPaths[node.Parent]);

            // Open folder int list view
            MainForm.Instance.CurrentTab.Folder = path;
        }

    }
}
