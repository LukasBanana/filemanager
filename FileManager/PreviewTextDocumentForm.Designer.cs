﻿namespace FileManager
{
    partial class PreviewTextDocumentForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textContent = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // textContent
            // 
            this.textContent.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textContent.BackColor = System.Drawing.SystemColors.Window;
            this.textContent.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textContent.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textContent.Location = new System.Drawing.Point(5, 5);
            this.textContent.Name = "textContent";
            this.textContent.ReadOnly = true;
            this.textContent.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Horizontal;
            this.textContent.Size = new System.Drawing.Size(550, 540);
            this.textContent.TabIndex = 0;
            this.textContent.Text = "< NO CONTENT >";
            this.textContent.WordWrap = false;
            // 
            // FormPreviewTextDocument
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.HotTrack;
            this.ClientSize = new System.Drawing.Size(560, 550);
            this.Controls.Add(this.textContent);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormPreviewTextDocument";
            this.Text = "FormPreviewTextDocument";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RichTextBox textContent;
    }
}