﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace FileManager
{
    class FolderListView : ListView
    {
        public FolderListView()
        {
            // Enable double buffering to prevent flickering
            SetStyle(ControlStyles.OptimizedDoubleBuffer | ControlStyles.AllPaintingInWmPaint, true);

            //ListViewItemSorter = ...
        }

        private bool AreDaysEqual(DateTime a, DateTime b)
        {
            return (a.Year == b.Year && a.Month == b.Month && a.Day == b.Day);
        }

        public string GetPrettyDate(DateTime date, ViewOptions options = null)
        {
            if (date != null)
            {
                // Setup view options for date
                if (options == null)
                    options = new ViewOptions();

                var format = "HH:mm";

                if (options.ShowSeconds)
                    format += ":ss";

                if (options.UsePrettyDay)
                {
                    // Check for today
                    if (AreDaysEqual(date, DateTime.Now))
                        return "Heute " + date.ToString(format);

                    // Check for yesterday
                    if (AreDaysEqual(date, DateTime.Now.AddDays(-1)))
                        return "Gestern " + date.ToString(format);
                }

                if (options.UseMonthNames)
                    format = "ddd d. MMM yyyy, " + format;
                else
                    format = "ddd dd.MM.yyyy, " + format;

                return date.ToString(format);
            }
            return "";
        }

        // Returns the file or folder size (in bytes) as pretty string
        public string GetPrettySize(long size, bool useKBOnly = true)
        {
            // Reduce size number
            var typeList = new string[]{ "Byte", "KB", "MB", "GB", "TB" };

            int idx = 0;
            while (size > 4096 && idx < typeList.Length)
            {
                size /= 1024;
                ++idx;
                if (useKBOnly)
                    break;
            }

            // Beautify number
            var s = "";

            while (true)
            {
                s = (size % 1000).ToString() + s;
                size /= 1000;
                if (size > 0)
                    s = "." + s;
                else
                    break;
            }

            return s + " " + typeList[idx];
        }

        private ListViewItem AppendListItem(string name, string dateModified, string typeName, string size, string permissions, ViewOptions options, ref bool oddIdx)
        {
            var backColor = Color.FromArgb(0xef, 0xf5, 0xfe);
            var subItemColor = Color.FromArgb(0x6d, 0x6d, 0x6d);

            var item = new ListViewItem(new string[] { name, dateModified, typeName, size, permissions });

            // Setup subitem colors
            item.UseItemStyleForSubItems = false;

            for (int i = 0; i < item.SubItems.Count; ++i)
            {
                if (oddIdx)
                    item.SubItems[i].BackColor = backColor;
                if (i > 0)
                    item.SubItems[i].ForeColor = subItemColor;
            }

            if (options.AlternateEntryColor)
                oddIdx = !oddIdx;

            return item;
        }

        private void AppendFolderEntry(string dir, ViewOptions options, ref bool oddIdx)
        {
            // Get directory information
            var info = new DirectoryInfo(dir);

            if (!options.ShowHiddenEntries && info.Attributes.HasFlag(FileAttributes.Hidden))
                return;

            // Get common properties
            var name = Path.GetFileName(dir);
            var dateModified = GetPrettyDate(Directory.GetLastWriteTime(dir), options);
            var type = "Ordner";
            var size = "";

            // Get permission properties
            var attribs = new EntryAttributes(info);

            // Add list item
            var item = AppendListItem(name, dateModified, type, size, attribs.ToString(), options, ref oddIdx);

            if (options.UseHighlight)
            {
                item.SubItems[0].ForeColor = options.FolderColor;
                //item.SubItems[0].Font = new Font(item.SubItems[0].Font, FontStyle.Bold);
            }

            Items.Add(item);
        }

        private void AppendFileEntry(string file, ViewOptions options, ref bool oddIdx)
        {
            // Get file information
            var info = new FileInfo(file);

            if (!options.ShowHiddenEntries && info.Attributes.HasFlag(FileAttributes.Hidden))
                return;

            // Get common properties
            var type = FileTypeDenoter.Instance.FindFileFormatAssociation(file);
            var name = (options.HideKnownFileExt && type != null ? Path.GetFileNameWithoutExtension(file) : Path.GetFileName(file));
            var dateModified = GetPrettyDate(File.GetLastWriteTime(file), options);
            var size = GetPrettySize(info.Length, options.ShowKBOnly);

            // Get permission properties
            var attribs = new EntryAttributes(info);

            if (type != null)
                attribs.Executable = type.executable;

            // Add list item
            var item = AppendListItem(name, dateModified, (type == null ? "" : type.typeName), size, attribs.ToString(), options, ref oddIdx);

            if (options.UseHighlight)
            {
                if (type != null && type.executable)
                    item.SubItems[0].ForeColor = options.ExecutableColor;
            }

            Items.Add(item);
        }

        public class FolderInfo
        {
            public int NumSubFolders { get; set; } = 0;
            public int NumFiles { get; set; } = 0;
        }

        // Fills this folder view with the content of the specified folder path
        public FolderInfo ShowFolderContent(string path, ViewOptions options = null)
        {
            if (options == null)
                options = ViewOptions.Global;

            // Clear previous items
            Items.Clear();

            try
            {
                bool oddIdx = false;

                // Enumerate directories and files
                var dirs = new List<string>(Directory.EnumerateDirectories(path));

                if (options.ShowExtFolders)
                {
                    dirs.Insert(0, "..");
                    dirs.Insert(0, ".");
                }

                var files = new List<string>(Directory.EnumerateFiles(path));

                // Append entries
                if (options.ShowFoldersFirst)
                {
                    foreach (var dir in dirs)
                        AppendFolderEntry(dir, options, ref oddIdx);
                    foreach (var file in files)
                        AppendFileEntry(file, options, ref oddIdx);
                }
                else
                {
                    dirs.AddRange(files);
                    dirs.Sort();
                    foreach (var dir in dirs)
                    {
                        if (Directory.Exists(dir))
                            AppendFolderEntry(dir, options, ref oddIdx);
                        else
                            AppendFileEntry(dir, options, ref oddIdx);
                    }
                }

                // Return folder information
                var folderInfo = new FolderInfo();

                folderInfo.NumSubFolders = dirs.Count;
                folderInfo.NumFiles = files.Count;

                return folderInfo;
            }
            catch (UnauthorizedAccessException e)
            {
                MessageBox.Show(e.ToString());
            }
            catch (PathTooLongException e)
            {
                MessageBox.Show(e.ToString());
            }

            return null;
        }

        private const float minFontSize = 7.0f;
        private const float maxFontSize = 20.0f;
        private const float fontResizeStep = 1.0f;
        private const float defaultFontSize = 9.0f;

        public void Zoom(float delta)
        {
            this.Font = new Font(this.Font.FontFamily, Math.Max(minFontSize, Math.Min(this.Font.Size + delta, maxFontSize)));
        }

        public void ZoomIn()
        {
            if (this.Font.Size < maxFontSize)
                this.Font = new Font(this.Font.FontFamily, this.Font.Size + fontResizeStep);
        }

        public void ZoomOut()
        {
            if (this.Font.Size > minFontSize)
                this.Font = new Font(this.Font.FontFamily, this.Font.Size - fontResizeStep);
        }

        public void ZoomReset()
        {
            if (this.Font.Size != defaultFontSize)
                this.Font = new Font(this.Font.FontFamily, defaultFontSize);
        }

    }
}
