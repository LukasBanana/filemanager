FileManager
===========

This file manager with tabs is intended to be an alternative to the Windows Explorer.

License
-------

[3-Clause BSD License](https://bitbucket.org/LukasBanana/filemanager/raw/d880c1acc222666e794ed9ef01a2951475b70b09/LICENSE.txt)


